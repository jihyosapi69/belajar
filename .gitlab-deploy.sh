#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Imagess'
echo "[*] Tag $WAKTU"
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker build -t jihyosapi69/belajardevops:$CI_COMMIT_BRANCH .
docker push jihyosapi69/belajardevops:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

# elif [ "$1" == "DEPLOY" ];then
# echo "[*] Tag $WAKTU"
# echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
# echo '[*] Generate SSH Identity'
# HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
# echo '[*] Execute Remote SSH'
# # bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker pull jihyosapi69/belajardevops:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker stop belajardevops-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker rm belajardevops-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker run -d -p 3071:80 --restart always --name belajardevops-$CI_COMMIT_BRANCH jihyosapi69/belajardevops:$CI_COMMIT_BRANCH"
# # ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai  "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
# echo $CI_PIPELINE_ID
fi
